import Vue from 'vue'
import Router from 'vue-router'



// aquí  componentes, las vistas se importan  abajo

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
   { path: "/characterSelect", name: "characterSelect", component:()=>import('../views/CharacterSelection.vue') }, 
   { path: "/battlefield", name: "battlefield", component:()=>import('../views/Battlefield.vue') }, 
   { path: "/pilaf", name: "pilaf", component:()=>import('../views/Pilaf.vue') }, 

   { path: "/shop", name: "shop", component:()=>import('../views/SatanShop.vue') }, 
   { path: "/wolfman", name: "wolfman", component:()=>import('../views/Wolfman.vue') }, 

  ]
})